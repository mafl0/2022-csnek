# CSNEK
Snake game programmed in C using ncurses text-mode graphics.

# TODOs

- Generation of prey iteratively produces a coordinate and tests wheter the snake is on that coordinate. This continues
  until a viable coordinate has been found. This wastes a lot of cycles if the snake occupies a large portion of the
  board. Also buggy since sometimes it spawns prey on top of the snake.
- A lot of the vlist functions are O(n) which we might be able to solve by either using a different data structure or by
  just being less naive about the implementation.

# WISHES

- A game like this ran on the nokia 3310, which has 1k of RAM and very little computing power. It should be possible to
  shrink the datastructures and logic of this game so that it becomes much smaller, and also less dependable on dynamic
  memory allocation i.e. no malloc.

