SRCDIR      = src
TESTSRCDIR  = $(SRCDIR)/test
OBJDIR      = build
TESTOBJDDIR = $(OBJDIR)/tests

SRCS := main.c draw.c game.c snake.c vlist.c
OBJS := $(addprefix $(OBJDIR)/, $(SRCS:.c=.o))
SRCS := $(addprefix $(SRCDIR)/, $(SRCS))

CC        = gcc

CFLAGS   += -Os
CFLAGS   += -Wall
CFLAGS   += -Wpedantic

LDFLAGS  += -L$(OBJDIR)
LDFLAGS  += -lcurses

CPPFLAGS += -I$(SRCDIR)

vpath %.c $(SRCDIR):$(TESTSRCDIR)
vpath %.h $(SRCDIR)
vpath %.o $(OBJDIR)

$(OBJDIR):
	mkdir -p $@

$(TESTOBJDIR): 
	mkdir -p $@

$(OBJDIR)/%.o: %.c | $(OBJDIR)
	$(CC) $(CPPFLAGS) $(CFLAGS) -o $@ -c $^

csnek: $(OBJS)
	$(CC) -o $@ $(LDFLAGS) $^

$(TESTOBJDIR)/testvlist: $(TESTSRCDIR)/testvlist.c
	$(CC) $(CFLAGS) -o $@ $^

.PHONY: clean
clean:
	rm -rfv $(OBJDIR)
	rm -f csnek

.PHONY: tags
tags:
	ctags -R -f tags $(SRCDIR)

.PHONY: all
all: tags $(OBJDIR)/snake $(OBJDIR)/test

