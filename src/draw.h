#ifndef __DRAW_H__
#define __DRAW_H__

GameT * gameDraw(WINDOW * window, GameT * game);
SnakeT * snakeDraw(WINDOW * window, SnakeT * snake);
GameT * preyDraw(WINDOW * window, GameT * game);



#endif // __DRAW_H_
