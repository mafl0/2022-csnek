#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>

#include <curses.h>

#include "vlist.h"
#include "snake.h"
#include "game.h"

/**
 * Generate a new prey on init or after one has been eaten. A new prey never
 * spawns on top of the snake. Trivial proof: When there are free squares the
 * game is not finished therefore when we need to continue generating prey,
 * there is always a square we can find for spawning one.
 *
 * TODO: fast if the snake is small, devastatingly slow when the snake occupies
 * almost every square on the field. if a field is WxH then the last square has
 * a probability of 1 / WxH of being picked immediately making it expected to
 * run WxH times before picking the last free square.
 *
 */
static GameT * generatePrey(GameT * game) {
  if (game == NULL)
    return game;

  do {
    game->prey->x = 1 + rand() % (game->width - 2);
    game->prey->y = 1 + rand() % (game->height - 2);
  } while (vlistMember(game->prey, game->snake->positions)); 

  return game;
}

/**
 * If the head is on the prey we eat it, generate a new prey and increase the 
 * score.
 */
static GameT * eat(GameT * game) {
  if (game == NULL)
    return game;

  if (vlistEq(game->snake->positions, game->prey)) {
    vlistAppend(game->prey->x, game->prey->y, &game->eaten);
    generatePrey(game);
    game->score++;
  } 

  return game;
}

/**
 * We grow based on the snakes `grow' field. 
 *
 * [1] If there is a vlist link on this field, we convert it to a new piece of 
 *     the snake and clear the field.
 * [2] Next, if the end of the tail is on the same coordinate as previously
 *     eaten prey, then we move that prey to the grow field so we may grow on
 *     the next game cycle.
 *
 * TODO: This smells like it can be done better and with less pointers
 */
static GameT * grow(GameT * game) {
  if (game == NULL)
    return game;

  // [1]
  if (!vlistEmpty(game->snake->grow)) {
    vlistAppend(game->snake->grow->x, game->snake->grow->y, &game->snake->positions);
    vlistDel(&game->snake->grow);
    game->snake->length++;
  }

  // [2]
  if (!vlistEmpty(game->eaten) && vlistEq(vlistLast(game->snake->positions), game->eaten)) {
      game->snake->grow       = game->eaten;
      game->eaten             = game->eaten->next;
      game->snake->grow->next = NULL;
  }

  return game;
}

/**
 * Game constructor.
 */
GameT * gameNew(SPACE_TYPE w, SPACE_TYPE h, void * window) {
  GameT * game = (GameT *) malloc(sizeof(GameT));

  if (errno == ENOMEM) {
    perror("Could not allocate memory for game");
    exit(errno);
  }

  game->width  = w;
  game->height = h;
  game->score  = 0;

  game->snake  = snakeNew(game->width, game->height);

  game->prey   = (VectListT *) malloc(sizeof(VectListT));

  if (errno == ENOMEM) {
    perror("Could not allocate memory for prey");
    exit(errno);
  }

  game->prey->next = NULL;

  game->eaten  = NULL;

  generatePrey(game);

  game->window = window;

  return game;
}

/**
 * Game destructor
 */
void gameDel(GameT * game) {
  if (game == NULL)
    return;

  if (game->snake != NULL)
    snakeDel(game->snake);

  if (!vlistEmpty(game->prey))
    vlistDel(&game->prey);

  free(game);
}

/**
 * Game step. In one game cycle we [1] move the snake, [2] eat prey if we happen
 * to catch any and [3] grow if we can.
 */
GameT * gameStep(GameT * game) {

  if (game == NULL)
    return game;

  // [1]
  snakeMove(game->snake);

  // [2,3]
  return grow(eat(game));
}

/**
 * Test whether the game is over. If the game is over we have either lost or
 * won. The lose conditions are [1] if we hit a wall or [2] if we hit ourselves.
 * The win condition is when [3] the snake occupies the whole board.
 */
bool gameOver(GameT * game) {
  
  if (game == NULL)
    return true;

  // head
  VectListT * head = game->snake->positions;

  // [1]
  if (head->x <= 0 || head->x >= game->width - 1 || head->y <= 0 || head->y >= game->height - 1) {
    return true;
  }
  
  // [2]
  for (VectListT * ptr = game->snake->positions->next; ptr != NULL; ptr = ptr->next) {
    if (vlistEq(head, ptr)) {
      return true;
    }
  }
  
  // [3]
  if (game->snake->length == (size_t) game->width * (size_t) game->height) {
    return true;
  }

  return false;
}
