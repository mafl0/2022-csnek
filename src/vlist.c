#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

#include <curses.h>

#include "vlist.h"

/**
 * Constructor
 */
VectListT * vlistNew(SPACE_TYPE x, SPACE_TYPE y) {
  VectListT * v = (VectListT *) malloc(sizeof(VectListT));

  v->x = x;
  v->y = y;
  v->next = NULL;

  return v;
}

/**
 * Destructor: Free the vlist. Recursively deletes other nodes if they exist.
 * Reset the pointer at *v to NULL.
 */
void vlistDel(VectListT ** v) {

  if (vlistEmpty(*v)) 
    return;

  if (!vlistEmpty((*v)->next))
    vlistDel(&((*v)->next));

  free(*v);
  *v = NULL;
}

/**
 * Empty list test. NULL pointer test in disguise. Safe to call when vs = NULL
 */
inline bool vlistEmpty(VectListT * vs) {
  return vs == NULL;
}

/**
 * Append a new position (x, y) to the end of a vlist vs.
 */
void vlistAppend(SPACE_TYPE x, SPACE_TYPE y, VectListT ** vs) {
  VectListT * v = vlistNew(x, y);
  VectListT * w = NULL;
  
  if (vlistEmpty(*vs)) {
    *vs = v;
  } else {
    w = vlistLast(*vs);
    w->next = v;
  }
}

/**
 * Test whether the singleton vlist v appears in the list vs. v is not required
 * to be singleton, but only the head of the list is checked.
 *
 * - Nothing is a member of the empty list
 * - The empty list is a member of any non-empty list (also by definition, since
 *   the last link can be considered `empty'
 */
bool vlistMember(VectListT * v, VectListT * vs) {
  bool ret = false;

  vlistPrint(vs);

  if (!vlistEmpty(vs)) {
    if (vlistEmpty(v)) {
      ret = true;
    } else {
      for (VectListT * ptr = vs; ptr != NULL; ptr = ptr->next) {
        if (vlistEq(v, vs)) {
          ret = true;
          break;
        }
      }
    }
  }

  return ret;
}

/**
 * Move a single vlist link in a specified direction. If the list is not a
 * singleton, only the head is moved.
 */
void vlistMove(VectListT * v, DirectionT dir) {
  if (vlistEmpty(v))
    return;

  switch (dir) {
    case N:
      v->y--;
      break;
    case W:
      v->x--;
      break;
    case S:
      v->y++;
      break;
    case E:
      v->x++;
      break;
  }
}

/**
 * Equality test on two vlist links. They are considered equal when their
 * coordinates are equal, or when they are both empty.
 */
bool vlistEq(VectListT * v, VectListT * w) {
  bool ret = false;

  if (vlistEmpty(v)) {
    if (vlistEmpty(w)) {
      ret = true; 
    } // else = always false
  } else {
    if (!vlistEmpty(w)) {
      ret = v->x == w->x && v->y == w->y; // test coordinates
    } // else = always false
  }

  return ret;
}

VectListT * vlistLast(VectListT * vs) {
  VectListT * ret = NULL;

  if (!vlistEmpty(vs)) {
    for (VectListT * v = vs; v != NULL; v = v->next)
      ret = v;
  }

  return ret;
}

size_t vlistLength(VectListT * vs) {
  size_t l = 0;

  if (!vlistEmpty(vs)) {
    while(!vlistEmpty(vs)) {
      ++l;
      vs = vs->next;
    }
  }

  return l;
}

VectListT * vlistIndex(VectListT * vs, size_t n) {
  VectListT * ptr = vs;

  for (size_t i = 0; i < n; ++i) {

    if (vlistEmpty(ptr))
      break;
    else
      ptr = ptr->next;
  }

  return ptr;
}

void vlistPrint(VectListT * vs) {
 
  size_t fieldlen = 14
       , buflen   = 2 + vlistLength(vs) * sizeof(char) * fieldlen
       ;

  char * buf = (char *) malloc(buflen);

  if (vlistEmpty(vs))
    return;

  for (VectListT * ptr = vs; ptr != NULL; ptr = ptr->next) {
    snprintf(buf, fieldlen, "[% 3d,% 3d] -> ", ptr->x, ptr->y);
  }
  snprintf(buf, 2, "\n");

  puts(buf);

  free(buf);
  
}
