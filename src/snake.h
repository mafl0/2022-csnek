#ifndef __SNAKE_H__
#define __SNAKE_H__

#define SNAKE_SYMBOL       'o'
#define PREY_SYMBOL        '+'
#define PREY_EATEN_SYMBOL  '@'

#define INITIAL_SNAKE_DIR E

typedef struct Snake {
  size_t length;               // Length of the snake
  DirectionT direction;        // Direction of snakes head
  struct VectList * positions; // List of positions of the snakes body
  struct VectList * grow;      // position to grow on the next turn
} SnakeT;

// Constructor and destructor for the snake
SnakeT * snakeNew(SPACE_TYPE width, SPACE_TYPE height);
void snakeDel(SnakeT * snake);

void snakeChangeDirection(SnakeT * snake, DirectionT dir);
SnakeT * snakeMove(SnakeT * snake);


#endif // __SNAKE_H__
