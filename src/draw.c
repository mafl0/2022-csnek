#include <curses.h>

#include "vlist.h"
#include "snake.h"
#include "game.h"
#include "draw.h"

/**
 * Drawing the game. We always start with a blank frame [1] and then draw the
 * snake and prey [2], the order is important because objects that are drawn
 * later overwrite other objects on the same coordinate.
 *
 * Later we 
 */
GameT * gameDraw(WINDOW * window, GameT * game) {

  if (window == NULL || game == NULL)
    return game;

  // [1]
  wclear(window);

  // [2]
  snakeDraw(window, game->snake);
  preyDraw(window, game);

  box(window, 0, 0);
  mvwprintw(game->window, 0, 2, " Score: % 3ld ", game->score);

  wrefresh(window);
  return game;
}

SnakeT * snakeDraw(WINDOW * window, SnakeT * snake) {
  
  if (window == NULL || snake == NULL)
    return snake;

  VectListT * ptr = snake->positions;

  while (ptr != NULL) {
    mvwaddch(window, ptr->y, ptr->x, SNAKE_SYMBOL);
    ptr = ptr->next;
  }

  return snake;
}


GameT * preyDraw(WINDOW * window, GameT * game) {
  
  if (window == NULL || game == NULL)
    return game;

  // Draw the prey we have eaten, as long as it coincides with the snake 
  if (!vlistEmpty(game->eaten)) {
    for (VectListT * ptr = game->eaten; ptr != NULL; ptr = ptr->next) {
      mvwaddch(window, ptr->y, ptr->x, PREY_EATEN_SYMBOL);
    }
  }

  // Draw the prey in the last bit of the tail, which causes a grow on the next 
  // step
  if (!vlistEmpty(game->snake->grow))
    mvwaddch(window, game->snake->grow->y, game->snake->grow->x, PREY_EATEN_SYMBOL);

  // Draw the prey we are currently chasing
  mvwaddch(window, game->prey->y, game->prey->x, PREY_SYMBOL);

  return game;
}

