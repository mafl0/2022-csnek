#ifndef __VLIST_H__
#define __VLIST_H__

#define SPACE_TYPE int

typedef enum Direction {
  N, // North
  E, // East
  S, // South
  W  // West
} DirectionT;

typedef struct VectList {
  SPACE_TYPE x;
  SPACE_TYPE y;
  struct VectList * next;
} VectListT;

VectListT * vlistNew(SPACE_TYPE x, SPACE_TYPE y);
void vlistDel(VectListT ** v);

void vlistAppend(SPACE_TYPE x, SPACE_TYPE y, VectListT ** v);
bool vlistMember(VectListT * v, VectListT * vs);
void vlistMove(VectListT * v, DirectionT dir);
bool vlistEq(VectListT * v, VectListT * w);
bool vlistEmpty(VectListT * vs);
VectListT * vlistIndex(VectListT * vs, size_t n);
VectListT * vlistLast(VectListT * vs);
size_t vlistLength(VectListT * vs);
void vlistPrint(VectListT * vs);

#endif // __VLIST_H__
