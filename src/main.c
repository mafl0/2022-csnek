#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>

#include <curses.h>

#include "vlist.h"
#include "snake.h"
#include "game.h"
#include "draw.h"

/**
 * help message
 */
void _usage() {
puts("csnek version 1.0\n"
"\n"
"-w <int>  width of the game field\n"
"-h <int>  height of the game field\n"
"-c <int>  speed of the game field in cycles/second. if this is <= 0, then the\n"
"          snake will not move on its own. Instead, it will wait for input from\n"
"          the player\n"
"-H        display this help"
);
}

/**
 * Handle game input
 */
void input(GameT * game, int keysym) {

  switch (keysym) {
    case KEY_UP:
      snakeChangeDirection(game->snake, N);
      break;
    case KEY_RIGHT:
      snakeChangeDirection(game->snake, E);
      break;
    case KEY_DOWN:
      snakeChangeDirection(game->snake, S);
      break;
    case KEY_LEFT:
      snakeChangeDirection(game->snake, W);
      break;

    default:
      break;
  }
}

int main(int argc, char ** argv) {

  // parse options
  int w                 = 20
    , h                 = 10
    , cycles_per_second =  5
    ;

  char option;

  while ((option = getopt(argc, argv, "w:h:c:H")) != -1) {
    switch(option) {
      case 'w':
        w = atoi(optarg);
        break;

      case 'h':
        h = atoi(optarg);
        break;

      case 'c':
        cycles_per_second = atoi(optarg);
        break;

      case 'H':
        _usage();
        return EXIT_SUCCESS;
        break;

      default:
        _usage();
        return EXIT_SUCCESS;
        break;
    }
  }

  // seed the RNG
  srand(time(NULL));

  // initialise ncurses
  initscr();
  cbreak();
  keypad(stdscr, TRUE);
  noecho();
  curs_set(0);

  // initialise game window
  WINDOW * gameWin;
  int xOffs = (COLS  - w)  / 2
    , yOffs = (LINES - h) / 2
    ;

  printw("Welcome to csnek. Press Ctrl-C tot exit.\n");
  refresh();

  gameWin = newwin(h, w, yOffs, xOffs);

  // initialise game
  GameT * game = gameNew(w, h, gameWin);
  gameDraw(game->window, game);

  // Game speed setting
  int timeout_ms = 0;
  timeout_ms = cycles_per_second <= 0 ? -1 : 1000 / cycles_per_second;

  // game loop
  while (1) {
    timeout(timeout_ms);
    input(game, getch());

    gameStep(game);
    gameDraw(game->window, game);

    if (gameOver(game)) {
      break;
    }
  }


  // Cleanup
  endwin();
  printf("Game over! Final score: % 3d with a snake length of % 3d\n", game->score, game->snake->length);
  gameDel(game);

  return EXIT_SUCCESS;
}
