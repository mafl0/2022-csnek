#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <assert.h>

#include "vlist.h"

VectListT * randomVList(size_t length) {
  VectListT * ret = NULL;
    
  for (size_t i = 0; i < length; ++i) {
    SPACE_TYPE x = rand()
             , y = rand()
             ;
    vlistAppend(x, y, &ret); // kind of using the tested function here...
  }

  return ret;
}

int main(int argc, char ** argv) {

  // seed the RNG
  srand(time(NULL));

  int    testcases   = 10000;
  size_t list_length = 0;
  size_t list_limit  = 1000;

  VectListT * head;

  for (size_t i = 0; i < testcases; ++i) {
    list_length = (rand() % list_limit) + 1;
    head = randomVList(list_length);

    printf("Generating test list with %ld elements\n", list_length);

    SPACE_TYPE x = rand()
             , y = rand()
             ;

    vlistAppend(x, y, &head);
  
    size_t list_length_after = vlistLength(head);
      
    assert(list_length_after == list_length + 1);
    printf("%ld + 1 = %ld\n", list_length, list_length_after);

    // cleanup
    vlistDel(&head);
  }

  return EXIT_SUCCESS;
}
