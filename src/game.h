#ifndef __GAME_H__
#define __GAME_H__

typedef struct Game {
  SPACE_TYPE width;          // Width of the game board
  SPACE_TYPE height;         // Height of the game board 

  uint64_t score;            // Amount of dots eaten

  struct VectList * prey;    // position of the prey
  struct VectList * eaten;   // Queue of eaten prey, for growing the snake
  
  SnakeT * snake;            // Representation of the snake

  void * window;             // Window to draw in

} GameT;

// Constructor and destructor for the game
GameT * gameNew(SPACE_TYPE w, SPACE_TYPE h, void * window);
void gameDel(GameT * game);

// Step and game-over conditions
GameT * gameStep(GameT * game);
bool gameOver(GameT * game);

#endif // __GAME_H__
