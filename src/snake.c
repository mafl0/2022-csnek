#include <stdlib.h>
#include <stdbool.h>

#include "vlist.h"
#include "snake.h"

/**
 * Constructor
 *
 * A new snake starts at the middle of the board with its head at (x,y) =
 * (game->width/2,game->height/2) and the rest of it's body preceding with
 * decreasing x-coordinates.
 *
 * The length of the initial snake is INITIAL_SNAKE_LEN or when the board
 * is too small game->width/2, whichever is smaller, but always at least 1.
 *
 * The initial snake is direction east on the board
 */
SnakeT * snakeNew(SPACE_TYPE width, SPACE_TYPE height) {
  SnakeT * snake = (SnakeT *) malloc(sizeof(SnakeT));

  snake->direction = INITIAL_SNAKE_DIR;
  snake->length    = 1;

  int64_t _initialX = width  >> 1
        , _initialY = height >> 1
        ;

  snake->positions = vlistNew(_initialX, _initialY);

  for (size_t i = 1; i < snake->length; ++i) 
    vlistAppend(_initialX - i, _initialY, &snake->positions);

  return snake;
}

/**
 * Destructor
 */
void snakeDel(SnakeT * snake) {
  
  if (snake == NULL)
    return;

  if (!vlistEmpty(snake->positions))
    vlistDel(&snake->positions);

  free(snake);
}

/**
 * dummy link, avoids a lot of dynamic memory stuff for snakeChangeDirection at
 * the cost of permanently embedding a VectListT struct in the .data section.
 */
static VectListT _dummy;

/**
 * Change direction.
 *
 * The only special case is when the snake is supposed to move `into' its own
 * body, breaking its neck. This is disallowed and the direction change will
 * simply be ignored. Only valid for snakes > 1.
 */

void snakeChangeDirection(SnakeT * snake, DirectionT dir) {

  if (snake == NULL)
    return;

  if (dir == snake->direction)
    return;

  if (snake->length > 1) {
    _dummy.x = snake->positions->x;
    _dummy.y = snake->positions->y;
    vlistMove(&_dummy, dir);

    if (vlistEq(snake->positions->next, &_dummy))
      return;
  }

  snake->direction = dir;
}

/**
 * Moving the snake a single step.
 *
 * If the snake is of length 1, we can simply move the only link in the linked
 * list into the direction that we are pointing.
 *
 * If the snake is longer than 1, then we have to be more clever. In fact, most
 * of the snake does not move between two game cycles: Only the head and the
 * last bit of the tail change. We can simply take the last bit of the tail and
 * make it the new head by moving it where the new head should go. Then we only
 * have to update some pointers to make sure the list structure stays consistent.
 *
 * TODO: O(n) in the length of the snake, we should optimize this. Probably
 * requires changing the snake data structure.
 *
 */
SnakeT * snakeMove(SnakeT * snake) {

  if (snake == NULL)
    return snake;

  // Do nothing for nonexistent snakes
  if (snake->length == 0)
    return snake;

  // simple case for length=1 snakes
  if (snake->length == 1) {
    vlistMove(snake->positions, snake->direction);

  // complex case for longer snakes
  } else {

    // Find some pointers
    VectListT * secondToLast = snake->positions;
    while (secondToLast->next->next != NULL)
      secondToLast = secondToLast->next;

    VectListT * last = secondToLast->next;

    // Make last the new head and move it to where the head is pointing
    last->next = snake->positions;
    last->x = snake->positions->x;
    last->y = snake->positions->y;
    vlistMove(last, snake->direction);

    // Make the secondToLast the new last
    secondToLast->next = NULL;

    // Update the head pointer
    snake->positions = last;
  }

  return snake;
}
