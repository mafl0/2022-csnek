define printVList
  if $argc == 1
    set var $hdPtr = $arg0

    set var $ptr = $hdPtr
    while $ptr != 0
      printVLink $ptr
      set var $ptr = $ptr->next
    end

  end
end

define printVLink
  if $argc == 1
    set var $ptr = $arg0
    
    printf "ptr  = %p\n", $ptr
    printf "x    = %d\n", $ptr->x
    printf "y    = %d\n", $ptr->y
    printf "next = %p\n", $ptr->next
    printf "last = %p\n", $ptr->last
    printf "\n"

  end
end
